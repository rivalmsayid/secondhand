<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_masuk</name>
   <tag></tag>
   <elementGuidId>0104a651-1234-445b-a359-6838b06803a1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input[name=&quot;commit&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@name='commit']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>99a51cbe-e444-48d8-aa8c-9ef2e03b63e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>996d572f-43b3-4f17-8584-fa2da692285c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>commit</value>
      <webElementGuid>87ba8db2-1adb-48d2-b5b0-6c87f7298ca5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Masuk</value>
      <webElementGuid>acd483cb-6be3-46bc-ad52-04f45611a404</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary w-100 rounded-4 p-3 fw-bold</value>
      <webElementGuid>2fb6a606-7623-4aaa-b56b-e135d988e449</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-disable-with</name>
      <type>Main</type>
      <value>Masuk</value>
      <webElementGuid>59ed0393-3958-4f01-a18b-1452afe6014e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;new_user&quot;)/div[@class=&quot;actions&quot;]/input[@class=&quot;btn btn-primary w-100 rounded-4 p-3 fw-bold&quot;]</value>
      <webElementGuid>5b3e4c5c-4398-4204-a4b3-09a0ba6792ed</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@name='commit']</value>
      <webElementGuid>f17873ca-7e5e-44ac-b564-fe6fc51f4fc1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='new_user']/div[3]/input</value>
      <webElementGuid>5677e029-e83a-45f6-b655-a7eb16040623</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/input</value>
      <webElementGuid>5b0889a9-63d9-4ca6-8b23-45efc4bbbe0e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'submit' and @name = 'commit']</value>
      <webElementGuid>1813ba7e-3e2f-4ce8-8f01-7e9db20f9bc8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
